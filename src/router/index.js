import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from '@/components/Dashboard'
import Profile from '@/components/Profile'
import Tables from '@/components/Tables'
import Maps from '@/components/Maps'
import BadGateway from '@/components/BadGateway'
import Login from '@/components/Login'
import DashboardLayout from '@/LayoutDashboard'
import ManMenu from '@/pages/ManajemenMenu'
import App from  '@/App.vue'
import MenuUtama from '@/pages/ManajemenMenu/menuUtama.vue'
import PageMenuTugas from '@/components/page_siswa/pageMenuTugas'
import PageListTugas from '@/components/page_siswa/pageMenuTugas/listTugas'
import PageMenuUjian from '@/components/page_siswa/pageMenuUjian'
import PageListUjian from '@/components/page_siswa/pageMenuUjian/listUjian'
import PageJawabSoal from '@/components/page_siswa/pageJawabSoal/jawabSoal'
import PageMateri from '@/components/page_siswa/pageMenuMateri'
import PageListMateri from '@/components/page_siswa/pageMenuMateri/listMateri'

import PageDaftarKelas from '@/components/daftarKelas'
import ManajemenTugas from '@/components/daftarKelas/manajemenTugas'
import ManajemenUjian from '@/components/daftarKelas/manajemenUjian'
import PageListSoal from '@/components/daftarKelas/listSoal'
import ManajemenGuru from '@/components/manajemenAkun/manajemenGuru'
import ManajemenSiswa from '@/components/manajemenAkun/manajemenSiswa'
import ManajemenKelas from '@/components/manajemenKelas'
import ManajemenMapel from '@/components/manajemenMapel'
import ManajemenNilai from '@/components/daftarKelas/manajemenNilai'
import listSoalDanEditNilaiEssai from '@/components/daftarKelas/listSoalDanEditNilaiEssai'
import MateriGuru from '@/components/materiguru'
import ListMapel from '@/components/materiguru/listMapel'
import ListMateri from '@/components/materiguru/listMapel/listMateri'
import PageListMapelGuru from '@/components/daftarKelas/listMapelTugasDanUjian'


Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'App',
      redirect:'/login',
      component: App,
      children:[
        {
          path: '/login',
          name: 'Login',
          component: Login,
          props: { page: 0 },
          alias: '/login'
        },
      ]
    },
    {
      path: '/dashboard',
      name: 'DashboardLayout',
      redirect:'/dashboard',
      component: DashboardLayout,
      children:[
        {
          path: '/dashboard',
          name: 'Dashboard',
          component: Dashboard,
          props: { page: 1 },
          alias: '/'
        },
        {
          path: '/profile',
          name: 'Profile',
          component: Profile,
          props: { page: 2 },
        },
        {
          path: '/manMenu',
          name: 'Manajemen Menu',
          component: ManMenu,
          props: { page: 3 },
        },
        {
          path: '/menuUtama',
          name: 'Menu Utama',
          component: MenuUtama,
          props: { page: 8 },
        },
        {
          path: '/menuTugas',
          name: 'Menu Tugas',
          component: PageMenuTugas,
          props: { page: 8 },
        },
        {
          path: '/listSoal',
          name: 'List Soal',
          component: PageListSoal,
          props: { page: 8 },
        },
        {
          path: '/listTugas',
          name: 'List Tugas',
          component: PageListTugas,
          props: { page: 8 },
        },
        {
          path: '/menuUjian',
          name: 'Menu Ujian',
          component: PageMenuUjian,
          props: { page: 9 },
        },
        {
          path: '/listUjian',
          name: 'List Ujian',
          component: PageListUjian,
          props: { page: 10 },
        },
        {
          path: '/daftarKelas',
          name: 'Daftar Kelas',
          component: PageDaftarKelas,
          props: { page: 11 },
        },
        {
          path: '/manajemenTugas',
          name: 'Manajemen Tugas',
          component: ManajemenTugas,
          props: { page: 11 },
        },
        {
          path: '/manajemenMapel',
          name: 'Manajemen Mapel',
          component: ManajemenMapel,
          props: { page: 11 },
        },
        {
          path: '/manajemenKelas',
          name: 'Manajemen Kelas',
          component: ManajemenKelas,
          props: { page: 11 },
        },
        {
          path: '/manajemenUjian',
          name: 'Manajemen Ujian',
          component: ManajemenUjian,
          props: { page: 12 },
        },
        {
          path: '/manajemenNilai',
          name: 'Manajemen Nilai',
          component: ManajemenNilai,
          props: { page: 12 },
        },
        {
          path: '/manajemenNilaiSiswa',
          name: 'Manajemen list Soal Dan Edit Nilai Essai',
          component: listSoalDanEditNilaiEssai,
          props: { page: 12 },
        },
        {
          path: '/materiGuru',
          name: 'Materi Pembelajaran',
          component: MateriGuru,
          props: { page: 12 },
        },
        {
          path: '/manajemenGuru',
          name: 'Manajemen Guru',
          component: ManajemenGuru,
          props: { page: 12 },
        },
        {
          path: '/manajemenSiswa',
          name: 'Manajemen Siswa',
          component: ManajemenSiswa,
          props: { page: 12 },
        },
        {
          path: '/pageListMateri',
          name: 'List Materi',
          component: PageListMateri,
          props: { page: 12 },
        },
        {
          path: '/pageListMapelGuru',
          name: 'List Mata Pelajaran',
          component: PageListMapelGuru,
          props: { page: 12 },
        },
        {
          path: '/pageMateri',
          name: 'Page Materi',
          component: PageMateri,
          props: { page: 12 },
        },
        {
          path: '/listMapel',
          name: 'List Mata Pelajaran',
          component: ListMapel,
          props: { page: 12 },
        },
        {
          path: '/listMateri',
          name: 'List Materi',
          component: ListMateri,
          props: { page: 12 },
        },
        {
          path: '/jawabSoal',
          name: 'Jawab Soal',
          component: PageJawabSoal,
          props: { page: 8 },
        },
        {
          path: '/tables',
          name: 'Tables',
          props: { page: 4 },
          component: Tables
        },
        {
          path: '/maps',
          name: 'Maps',
          props: { page: 5 },
          component: Maps
        },
        {
          path: '/404',
          name: 'BadGateway',
          props: { page: 6 },
          component: BadGateway
        },
        {
          path: '*',
          props: { page: 7 },
          redirect: '/404'
        }
      ]
    }
  ]
})