import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbvue/lib/css/mdb.min.css'
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/index.js'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueElementLoading from 'vue-element-loading'
import FileSelector from 'vue-file-selector';
import VCalendar from 'v-calendar';
import Calendar from 'v-calendar/lib/components/calendar.umd'
import DatePicker from 'v-calendar/lib/components/date-picker.umd'
import moment from 'moment';
 
// then use it!
Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('MM/DD/YYYY hh:mm')
  }
});
Vue.use(FileSelector);
Vue.use(VCalendar, {
  componentPrefix: 'vc'
});
Vue.component('calendar', Calendar)
Vue.component('date-picker', DatePicker)

Vue.use(VueGoogleMaps, {
  load: {
    libraries: 'places'
  }
});
Vue.component('VueElementLoading', VueElementLoading);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
