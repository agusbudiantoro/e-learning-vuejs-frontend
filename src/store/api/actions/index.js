import { Action } from '../types';
import Axios from 'axios';
import router from '../../../router/index.js'

var ip = 'http://backendrest.e-learningsmpn110jakarta.my.id/controller/api/v1/';

export default{

    //login
     async [Action.login]({dispatch},isi){
        console.log(isi);
        console.log(dispatch);
        return new Promise(async (resolve,reject) => {
            Axios.post(ip+'login',{
                "identitas_pengguna":isi.identitas,
                "password":isi.password,
            }).then(res => {
                console.log(res);
                localStorage.setItem('token', res.data.token);
                localStorage.setItem('id_user', res.data.id_pengguna);
                if(res.data.role == 2){
                    localStorage.setItem('nomor_identitas', res.data.nip);
                    localStorage.setItem('id_mapel', res.data.id_mapel);
                } else if(res.data.role == 1){
                    localStorage.setItem('nomor_identitas', res.data.nip);
                } else {
                    localStorage.setItem('nomor_identitas', res.data.nis);
                    localStorage.setItem('id_kelas', res.data.id_kelas);
                }
                localStorage.setItem('role', res.data.role);
                localStorage.setItem('nama', res.data.nama);
                // router.push({ path: 'dashboard' });
                resolve(res);
            }).catch(err=>{
                reject(err);
            })
        })
    },

    // mapel
    [Action.getMapelByidKelas]({dispatch},id_kelas){
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getMapelByIdKelas/'+id_kelas,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getAllMapel]({dispatch},){
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getAllMapel',{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.postMapel]({dispatch},data){
        console.log(data);
        return new Promise((resolve,reject)=>{
            Axios.post(ip+'tambahMapel',data,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.putMapel]({dispatch},data){
        return new Promise((resolve,reject)=>{
            Axios.put(ip+'putMapel/'+data.id_master_mapel,data,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.deleteMapel]({dispatch},data){
        return new Promise((resolve,reject)=>{
            Axios.delete(ip+'deleteMapel/'+data.id_master_mapel,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    //materi
    [Action.postMateri]({dispatch},data){
        console.log(data);
        return new Promise((resolve,reject)=>{
            var formdata = new FormData()
            formdata.append('id_kelas', data.id_kelas)
            formdata.append('id_mapel', data.id_mapel)
            formdata.append('judul', data.judul)
			formdata.append('file', data.file)
            Axios.post(ip+'tambahMateri',formdata,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.putMateri]({dispatch},data){
        console.log(data);
        return new Promise((resolve,reject)=>{
            var formdata = new FormData()
            formdata.append('id_materi', data.id_materi)
            formdata.append('judul', data.judul)
			formdata.append('file', data.file)
            Axios.put(ip+'editMateri',formdata,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.deleteMateri]({dispatch},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(ip+'deleteMateri/'+id,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getMateriByIdKelasIdMapel]({dispatch},data){
        console.log(data);
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getMateriByIdKelasIdMapel/'+data.id_kelas+'/'+data.id_mapel,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    // kelas
    [Action.postKelas]({dispatch},data){
        console.log(data);
        return new Promise((resolve,reject)=>{
            Axios.post(ip+'tambahKelas',data,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.putKelas]({dispatch},data){
        return new Promise((resolve,reject)=>{
            Axios.put(ip+'putKelas/'+data.id_kelas,data,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.deleteKelas]({dispatch},data){
        return new Promise((resolve,reject)=>{
            Axios.delete(ip+'deleteKelas/'+data.id_kelas,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getKelasByIdMapel]({dispatch},id_mapel){
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getKelasByIdMapel/'+id_mapel,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getAllKelas]({dispatch},id_mapel){
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getAllKelas/',{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    //tabel list kelas guru
    [Action.getListKelasGuru]({dispatch},data){
        // console.log(data);
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getListKelasGuruByIdGuru/'+data.id_guru,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getListKelasGuruForGuru]({dispatch},data){
        // console.log(data);
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getListKelasGuruByIdGuruForGuru/'+data.id_guru,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getListMapelKelasGuruByIdKelas]({dispatch},data){
        // console.log(data);
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getListMapelKelasGuruByIdKelas/'+data.id_guru+'/'+data.id_kelas,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.tambahListKelasGuru]({dispatch},data){
        // console.log(data);
        return new Promise((resolve,reject)=>{
            Axios.post(ip+'tambahListKelasGuru/',data,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.putListKelasGuru]({dispatch},data){
        // console.log(data);
        return new Promise((resolve,reject)=>{
            Axios.put(ip+'editListKelasGuru/'+data.id_list_kelas,data,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.deleteListKelasGuru]({dispatch},data){
        // console.log(data);
        return new Promise((resolve,reject)=>{
            Axios.delete(ip+'deleteListKelasGuru/'+data.id_list_kelas,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    //tugas
    [Action.getTugasByIdMapelAndStatus]({dispatch},data){
        console.log(data);
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getTugasByIdMapel/'+data.id+'/'+localStorage.getItem('id_user')+"/"+data.status,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getTugasByIdTugas]({dispatch},id_nama_tugas){
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getTugasByIdTugas/'+id_nama_tugas,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getTugasByIdKelasAndMapel]({dispatch},isi){
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getTugasByIdKelas/'+isi.id+"/"+isi.id_mapel+"/"+isi.status,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.postTugas]({dispatch},data){
        console.log(data);
        return new Promise((resolve,reject)=>{
            let isi = {
                nama_tugas:data.nama_tugas,
                id_mapel:data.id_mapel,
                durasi:data.durasi,
                status_tugas:data.status_tugas,
                id_kelas:data.id_kelas
            }
            Axios.post(ip+'tambahTugas',isi,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.putTugas]({dispatch},data){
        console.log(data);
        return new Promise((resolve,reject)=>{
            let isi = {
                nama_tugas:data.nama_tugas,
                durasi:data.durasi,
                id_nama_tugas:data.id_nama_tugas
            }
            Axios.put(ip+'putTugasById',isi,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.deleteTugas]({dispatch},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(ip+'deleteTugasById/'+id,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    //nilai
    [Action.postNilai]({dispatch},data){
        return new Promise((resolve,reject)=>{
            let isi = {
                id_siswa:localStorage.getItem('id_user'),
                id_tugas:data.id_tugas,
                id_kelas:data.id_kelas,
                id_mapel:data.id_mapel,
                nilai:data.nilai,
                waktu_mulai:data.waktu_mulai,
                waktu_selesai:data.waktu_selesai,
                jawaban_siswa:data.jawaban_siswa
            }
            Axios.post(ip+'tambahNilai',isi,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.putNilaiKeseluruhan]({dispatch},data){
        return new Promise((resolve,reject)=>{
            let isi = {
                id_siswa:data.id_siswa,
                id_tugas:data.id_tugas,
                nilai_keseluruhan:data.nilai_keseluruhan
            }
            Axios.put(ip+'putNilaiKeseluruhan',isi,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getNialiByIdKelasIdMapelIdTugas]({dispatch},data){
        console.log(data);
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getNilai/'+data.id_kelas+"/"+data.id_mapel+"/"+data.id_tugas,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getNilaiByIdSiswaAndIdTugas]({dispatch},data){
        console.log(data);
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getNilaiByidSiswadantugas/'+data.id_siswa+"/"+data.id_tugas,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    // status tugas
    [Action.postStatusTugas]({dispatch},data){
        console.log(data);
        return new Promise((resolve,reject)=>{
            let isi = {
                id_siswa:localStorage.getItem('id_user'),
                id_tugas:data.id_nama_tugas,
                waktu_mulai:data.waktu_mulai,
                status:data.status_tugas,
                waktu_selesai:data.waktu_selesai
            }
            Axios.post(ip+'tambahStatusTugas',isi,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getStatusTugas]({dispatch},id_tugas){
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getStatusTugasByIdTugasDanSiswa/'+id_tugas+'/'+localStorage.getItem('id_user'),{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getStatusTugasForGuru]({dispatch},isi){
        console.log(isi);
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getStatusTugasByIdTugasDanSiswa/'+isi.id_tugas+'/'+isi.id_siswa,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    //guru
    [Action.getAllAkunGuru]({dispatch},){
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getAllGuru',{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.registerGuru]({dispatch},data){
        return new Promise((resolve,reject)=>{
            Axios.post(ip+'registerGuru',data,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.putAkunGuru]({dispatch},data){
        return new Promise((resolve,reject)=>{
            Axios.put(ip+'putGuru/'+data.id_guru+"/"+data.id_akun,data,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    
    [Action.changePassword]({dispatch},data){
        console.log(data);
        return new Promise((resolve,reject)=>{
            Axios.put(ip+'changePassword/'+data.id_akun,data,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    //siswa
    [Action.getAllDataSiswa]({dispatch},){
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getAllDataSiswa',{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.registerSiswa]({dispatch},data){
        return new Promise((resolve,reject)=>{
            Axios.post(ip+'registerSiswa',data,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.editSiswa]({dispatch},data){
        return new Promise((resolve,reject)=>{
            Axios.put(ip+'putSiswa/'+data.id_siswa+"/"+data.id_akun,data,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    //akun
    [Action.delAkun]({dispatch},data){
        return new Promise((resolve,reject)=>{
            Axios.delete(ip+'delAkun/'+data.id_akun,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    // soal
    [Action.putSoal]({dispatch},data){
        console.log(data);
        return new Promise((resolve,reject)=>{
            let isi = {
                "soal":data.soal,
                "jawaban":data.jawaban,
                "type_soal":data.type_soal,
                "no_urut_soal":data.no_urut_soal,
                "kunci":data.kunci,
                "file":data.file,
                'point':data.point
            }
            Axios.put(ip+'putSoalByIdSoal/'+data.id_soal,isi,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.deleteSoal]({dispatch},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(ip+'deleteSoalByIdSoal/'+id,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getSoalByIdTugas]({dispatch},isi){
        return new Promise((resolve,reject)=>{
            let body = {
                id_tugas:isi.id_nama_tugas,
                id_kelas:isi.id_kelas
            }
            Axios.get(ip+'getSoalByIdTugas/'+isi.id_nama_tugas+"/"+isi.id_kelas,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.postSoal]({dispatch},isi){
        return new Promise((resolve,reject)=>{
            let body ={
                id_pelajaran:isi.id_mapel,
                id_kelas:isi.id_kelas,
                id_tugas:isi.id_tugas,
                soal:isi.soal,
                jawaban:isi.jawaban,
                type_soal:isi.type_soal,
                no_urut_soal:isi.no_urut_soal,
                kunci:isi.kunci,
                file:isi.file,
                point:isi.point
            }
            Axios.post(ip+'tambahSoal',body,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    //kunci jawaban
    [Action.getKunciJawabanByIdSoal]({dispatch},id){
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getKunciByIdSoal/'+id,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    //file
    [Action.postFile]({dispatch},data){
        return new Promise((resolve,reject)=>{
            console.log(data);
            var formdata = new FormData()
			formdata.append('file', data)
            Axios.post(ip+'uploadFile',formdata,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                    "Content-Type": 'multipart/form-data'
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.deleteFile]({dispatch},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(ip+'delFile/'+id,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token')
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getFile]({dispatch},id){
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getFile/'+id,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token')
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    //jawaban siswa
    [Action.postJawabanSiswa]({dispatch},data){
        return new Promise((resolve,reject)=>{
            Axios.post(ip+'tambahJawabanSiswa',data,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },
    [Action.getJawabanSiswaBySoal]({dispatch},isi){
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'getJawabanSiswa/'+isi.id_soal+"/"+isi.id_siswa,{
                headers: {
                    "Authorization": "Bearer "+localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    //menu bot

    // get
    [Action.getMenuBot](){
        return new Promise((resolve,reject)=>{
            Axios.get(ip+'api/menubot/getall',{
                headers: {
                    "x-access-token": localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    //delete
    [Action.deleteMenuBot]({dispatch},id){
        return new Promise((resolve,reject)=>{
            console.log(id);
            Axios.delete(ip+'api/menubot/'+id,{
                headers: {
                    "x-access-token": localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    // put
    [Action.putMenuBot]({dispatch},data){
        return new Promise((resolve,reject)=>{
            Axios.put(ip+'api/menubot',data,{
                headers: {
                    "x-access-token": localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    },

    //post
    [Action.postMenuBot]({dispatch},data){
        return new Promise((resolve,reject)=>{
            Axios.post(ip+'api/menubot',{
                "judul": data.judul,
                "keyword": data.keyword,
                "teks": data.teks,
                "idmenu": data.idmenu,
                "statusmenu": data.statusmenu,
                "idfungsi": 0,
                "status": 1,
            },{
                headers: {
                    "x-access-token": localStorage.getItem('token'),
                }
            }).then((res)=>{
                resolve(res)
            }).catch((err)=>{
                reject(err);
            })
        })
    }
}