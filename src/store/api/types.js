export const Action = {
    login:"login",

    // kelas
    getKelasByIdMapel:"getKelasByIdMapel",
    getAllKelas:"getAllKelas",
    postKelas:"postKelas",
    putKelas:"putKelas",
    deleteKelas:"deleteKelas",

    //mapel
    getMapelByidKelas:"getMapelByidKelas",
    getAllMapel:"getAllMapel",
    postMapel:"postMapel",
    putMapel:"putMapel",
    deleteMapel:"deleteMapel",

    //tugas
    getTugasByIdMapelAndStatus:"getTugasByIdMapelAndStatus",
    getTugasByIdTugas:"getTugasByIdTugas",
    getTugasByIdKelasAndMapel:"getTugasByIdKelasAndMapel",
    postTugas:"postTugas",
    putTugas:"putTugas",
    deleteTugas:"deleteTugas",

    //materi
    getMateriByIdKelasIdMapel:'getMateriByIdKelasIdMapel',
    postMateri:'postMateri',
    putMateri:'putMateri',
    deleteMateri:'deleteMateri',

    //nilai
    postNilai:"postNilai",
    getNialiByIdKelasIdMapelIdTugas:"getNialiByIdKelasIdMapelIdTugas",
    getNilaiByIdSiswaAndIdTugas:"getNilaiByIdSiswaAndIdTugas",
    putNilaiKeseluruhan:'putNilaiKeseluruhan',

    // statusTugas
    postStatusTugas:"postStatusTugas",
    getStatusTugas:"getStatusTugas",
    getStatusTugasForGuru:'getStatusTugasForGuru',

    // soal
    getSoalByIdTugas:"getSoalByIdTugas",
    postSoal:"postSoal",
    putSoal:"putSoal",
    deleteSoal:"deleteSoal",

    //kunci jawaban
    getKunciJawabanByIdSoal:"getKunciJawabanByIdSoal",

    //file
    postFile:"postFile",
    deleteFile:"deleteFile",
    getFile:"getFile",

    // jawaban siswa
    postJawabanSiswa:"postJawabanSiswa",
    getJawabanSiswaBySoal:"getJawabanSiswaBySoal",

    //guru
    getAllAkunGuru:"getAllAkunGuru",
    registerGuru:"registerGuru",
    putAkunGuru:"putAkunGuru",
    changePassword:"changePassword",
    
    //siswa
    getAkunSiswa:"getAkunSiswa",
    getAllDataSiswa:"getAllDataSiswa",
    registerSiswa:"registerSiswa",
    editSiswa:"editSiswa",

    //akun 
    delAkun:"delAkun",

    getMenuBot:'getMenuBot',
    deleteMenuBot:'deleteMenuBot',
    putMenuBot:'putMenuBot',
    postMenuBot:'postMenuBot',

    //list kelas guru
    getListKelasGuru:'getListKelasGuru',
    getListKelasGuruForGuru:'getListKelasGuruForGuru',
    getListMapelKelasGuruByIdKelas:'getListMapelKelasGuruByIdKelas',
    tambahListKelasGuru:'tambahListKelasGuru',
    putListKelasGuru:'putListKelasGuru',
    deleteListKelasGuru:'deleteListKelasGuru'
}