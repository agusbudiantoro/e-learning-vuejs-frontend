import Vue from 'vue'
import Vuex from 'vuex'
import api from './api'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules:{
    api
  },
  state: {

  },
  mutations: {

  },
  actions: {

  }
})
